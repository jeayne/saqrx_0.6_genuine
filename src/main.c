/*
 *      #####################################################################
 *
 *        Copyright (C) 2006  Johan H. Bodin SM6LKM
 *
 *        This software is provided 'as is', without warranty of any kind,
 *        express or implied. In no event shall the author be held liable
 *        for any damages arising from the use of this software.
 *
 *        Permission to use, copy, modify, and distribute this software and
 *        its documentation for non-commercial purposes is hereby granted,
 *        provided that the above copyright notice and this disclaimer appear
 *        in all copies and supporting documentation.
 *
 *        The software must NOT be sold or used as part of a any commercial
 *        or "non-free" product.
 *
 *      #####################################################################
 */

/*------------------------------------------------------------------------------
 *
 *      SM6LKM's SAQ Receiver
 *
 *      My first "real" Window$ application!
 *
 *      Revision history:
 *        2006-12-04:
 *          "Serious" work begins... /JHB
 *        2006-12-05:
 *          Figured out how to do off-screen painting to avoid flicker. /JHB
 *        2006-12-06:
 *          Removed the hi-prio worker thread and moved it all into the callback.
 *          Added some mouse support including scroll wheel for tuning. /JHB
 *        2006-12-07:
 *          Added some "homebrew" buttons (LKM's non-standard UI :~)
 *          Corrected spectrum Y scaling for Hanning window.
 *          Added text to About & Help boxes. /JHB
 *        2006-12-08:
 *          My first Windows app' is ready for SAQ's X-mas transmission!
 *
 *        2006-12-11:
 *          Some finishing touches to the help text. First release (v0.3)!
 *
 *        2006-12-12:
 *          If host error on start-up, show the error code. Second release (v0.4)
 *          Fixed icon linkage, thanks Wolf!
 *
 *        2006-12-13, v0.5wip:
 *          Tuning range changed:
 *            LO range with WIDE filter: 0..19050 Hz
 *            LO range with NARROW filter: 0..20650 Hz
 *            (LO range was fixed at 0..19000 Hz in previous versions)
 *
 *        2006-12-14, v0.5 released:
 *          - Added 300Hz filter
 *          - Added CPU load indicator (toggled on/off with 'C').
 *          - Added 1Hz keyboard tuning with 'U'/'D' for up/down.
 *
 *        2006-12-14, v0.5a released:
 *          Icon buddy eyeglobes are now non-transparent ;o). /JHB
 *
 *        2006-12-19, v0.5b (NOT RELEASED):
 *          - FFT buffer size reduced to FFT_SIZE_IN (1024).
 *          - Plot buffer size reduced to NFFTPLOTPOINTS (FFT_SIZE_IN/2+1 = 513).
 *          - Unused threadID variable removed. /JHB
 *
 *        2007-02-18, v0.6 (NOT RELEASED):
 *          - Added "SAQrx alredy running" check. /JHB
 *
 *      FIXMEs:
 *        - Use some non-modal window for help text instead of messagebox()...
 */

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <conio.h>
#include "pa_host.h"
#include "portaudio.h"
#include "dspmath.h"
#include "coeffs.h"

#define FS            44100.0
#define FPB           256
#define FFT_SIZE_IN   1024    // time domain input points for the FFT
#define NFFTPLOTPOINTS (FFT_SIZE_IN/2 + 1)  // number of bins in FFT output

#define START_LO_FREQ 16500.0
#define MIN_LO_FREQ   0.0
#define MAX_LO_FREQ_NARROW  21100.0
#define MAX_LO_FREQ_MEDIUM  20650.0
#define MAX_LO_FREQ_WIDE    19050.0

#define SMOOTH_BINS
#define SMOOTH_K    0.333333

float g_fltFftBufRe[FFT_SIZE_IN];
float g_fltFftBufIm[FFT_SIZE_IN];
float g_fltPlotFftV[NFFTPLOTPOINTS];

float g_rLoFrequency = START_LO_FREQ;
float g_rLoPhInc = (START_LO_FREQ * 2.0 * C_PI / FS);

#define MAXOUTPUTGAINSTEP 5
int   g_iOutputGainStep = 2;
float g_rOutputGainFactor = 10.0;

volatile BOOL g_qGUIFinishedPlotting = TRUE;
volatile int q_OverLoad = 0;
PortAudioStream *g_PAStream;
int g_iFilterBW = 1;                        // 0-1-2 => NARROW-MEDIUM-WIDE
int g_qShowCpuLoad = 0;
volatile float g_rCpuLoad;


/*------------------------------------------------------------------------------
 *
 *      Audio stream callback function
 *
 *      All audio processing is done in this function!
 *
 *      (No need to mess with a separate worker thread since this is
 *      already taken care of by PortAudio!)
 */

static int AudioCallback (void *inputBuffer,
                          void *outputBuffer,
                          unsigned long framesPerBuffer,
                          PaTimestamp outTime,
                          void *userData)
{
  unsigned long frameCnt;
  float *out = (float *) outputBuffer;
  float *in = (float *) inputBuffer;
  float leftInput, rightInput;

  float rI44k, rQ44k;
  float rI11k, rQ11k;
  float rTmp;
  float re,im;
  int i;

  static float rLocalOscPhAcc;

  static int iFftBufIdxIn = 0;
  static unsigned int ui44kSampleCounter = 0;

  static int iDecimatorIdxIn = 0;
  static float rDecimatorQueueI[64];
  static float rDecimatorQueueQ[64];

  static int iMainFilterIdxIn = 0;
  static float rMainFilterQueueI[FIR_LARGEST_LEN];
  static float rMainFilterQueueQ[FIR_LARGEST_LEN];

  static int iInterpolatorIdxIn = 0;
  static float rInterpolatorQueueI[16];
  static float rInterpolatorQueueQ[16];

  // Argument inputBuffer may be NULL during start-up so...

  if (inputBuffer == NULL)
    return 0;

  // Read input buffer, process data, and fill output buffer.

  for (frameCnt = 0; frameCnt < framesPerBuffer; frameCnt++)
  {
    // Get interleaved soundcard samples from input buffer

    leftInput = *in++;
    rightInput = *in++; // right channel not used but we must pick it anyway

    // Check peak signal level

    rTmp = leftInput;
    if (rTmp < 0)         // faster than calling abs()?
      rTmp = -rTmp;
    if (rTmp > 0.891251)  // 1 dB below full-scale (1.0)
      q_OverLoad = 1;

    // Run local oscillator (NCO)

    rLocalOscPhAcc += g_rLoPhInc;
    if (rLocalOscPhAcc > (2.0 * C_PI))
      rLocalOscPhAcc -= (2.0 * C_PI);

    // Half complex mixer

    rI44k = leftInput * cos (rLocalOscPhAcc);
    rQ44k = leftInput * sin (rLocalOscPhAcc);

//########################################
// FAKED SIGNAL FOR FFT Y SCALING TESTS:
//    leftInput = cos (rLocalOscPhAcc); // amplitude 1.0
//########################################

    // Decimation filter, R = 4, nTaps = 64 (hardcoded... ;-)

    if (--iDecimatorIdxIn < 0)  // fill buffer backwards (that convolution stuff...)
      iDecimatorIdxIn = 63;     // (well, the filter is symmetrical, but anyway...)

    rDecimatorQueueI[iDecimatorIdxIn] = rI44k;
    rDecimatorQueueQ[iDecimatorIdxIn] = rQ44k;

    // Decimate Fs and do the heavy stuff at lower speed

    if (++ui44kSampleCounter >= 4)  // decimation factor = R = 4
    {
      ui44kSampleCounter = 0;

      // Fs = 11025 Hz code goes here, first decimation filter:

      rI11k = rQ11k = 0.0;          // clear MAC accumulators
      for (i = 0; i < 64; i++)
      {
        rTmp = rDecIntCoeffs[i];
        rI11k += rDecimatorQueueI[iDecimatorIdxIn] * rTmp;
        rQ11k += rDecimatorQueueQ[iDecimatorIdxIn] * rTmp;
        if (++iDecimatorIdxIn >= 64)
          iDecimatorIdxIn = 0;
      }

      rI11k *= 1.0e-8;      // normalize (FIR gain ~10^8 !!!)
      rQ11k *= 1.0e-8;

      // rI11k and rQ11k now contains the downsampled complex signal
      // ready for processing at Fs = 11025 Hz. Passband = +/-3000Hz
      // with transition bands out to +/-5512.5Hz (stopband).

      // Main selectivity filters

      if (g_iFilterBW == 0) // NARROW?
      {
        if (--iMainFilterIdxIn < 0)
          iMainFilterIdxIn = FIR_LEN_CW300H-1;
        rMainFilterQueueI[iMainFilterIdxIn] = rI11k;
        rMainFilterQueueQ[iMainFilterIdxIn] = rQ11k;
        rI11k = rQ11k = 0.0;          // clear MAC accumulators
        for (i = 0; i < FIR_LEN_CW300H; i++)
        {
          rI11k += rMainFilterQueueI[iMainFilterIdxIn] * rCw300HCoeffs_I[i];
          rQ11k += rMainFilterQueueQ[iMainFilterIdxIn] * rCw300HCoeffs_Q[i];
          if (++iMainFilterIdxIn >= FIR_LEN_CW300H)
            iMainFilterIdxIn = 0;
        }
        rI11k *= 1.0e-8;      // normalize (FIR gain ~10^8 !!!)
        rQ11k *= 1.0e-8;
      }
      else
      if (g_iFilterBW == 1) // MEDIUM?
      {
        if (--iMainFilterIdxIn < 0)
          iMainFilterIdxIn = FIR_LEN_CW1K-1;  // <= forgot -1... whattanuglybug!
        rMainFilterQueueI[iMainFilterIdxIn] = rI11k;
        rMainFilterQueueQ[iMainFilterIdxIn] = rQ11k;
        rI11k = rQ11k = 0.0;          // clear MAC accumulators
        for (i = 0; i < FIR_LEN_CW1K; i++)
        {
          rI11k += rMainFilterQueueI[iMainFilterIdxIn] * rCw1kCoeffs_I[i];
          rQ11k += rMainFilterQueueQ[iMainFilterIdxIn] * rCw1kCoeffs_Q[i];
          if (++iMainFilterIdxIn >= FIR_LEN_CW1K)
            iMainFilterIdxIn = 0;
        }
        rI11k *= 1.0e-8;      // normalize (FIR gain ~10^8 !!!)
        rQ11k *= 1.0e-8;
      }
      else  // g_iFilterBW == 2 => WIDE!
      {
        if (--iMainFilterIdxIn < 0)
          iMainFilterIdxIn = FIR_LEN_SSB2K4-1;
        rMainFilterQueueI[iMainFilterIdxIn] = rI11k;
        rMainFilterQueueQ[iMainFilterIdxIn] = rQ11k;
        rI11k = rQ11k = 0.0;          // clear MAC accumulators
        for (i = 0; i < FIR_LEN_SSB2K4; i++)
        {
          rI11k += rMainFilterQueueI[iMainFilterIdxIn] * rSsb2k4Coeffs_I[i];
          rQ11k += rMainFilterQueueQ[iMainFilterIdxIn] * rSsb2k4Coeffs_Q[i];
          if (++iMainFilterIdxIn >= FIR_LEN_SSB2K4)
            iMainFilterIdxIn = 0;
        }
        rI11k *= 1.0e-8;      // normalize (FIR gain ~10^8 !!!)
        rQ11k *= 1.0e-8;
      }

      // "Summing point" of the "Phasing Method Receiver"

      rI11k += rQ11k;       // select USB (subtract for LSB)
      rQ11k = rI11k;        // same signal to both ears (mono output)

    // Feed a stereo 11k sample to the interpolation filter
    // (two separate channels are used in case we want
    // stereo output in the future for binaural I/Q etc.)

      iInterpolatorIdxIn--;   // backward input, remember that convolution integral...
      if (iInterpolatorIdxIn < 0)
        iInterpolatorIdxIn = 15;
      rInterpolatorQueueI[iInterpolatorIdxIn] = rI11k;
      rInterpolatorQueueQ[iInterpolatorIdxIn] = rQ11k;
    } // end of if (++ui44kSampleCounter >= 4)

    // Now we're back in Fs = 44100 Hz business again

    // Run interpolation filter at Fs = 44k1
    //
    // Note! The interpolator uses the same 64-tap FIR as the decimator but
    // since 3 out of 4 input samples are zero, only 16 MAC operations need
    // to be computed. This is the reason why the interpolator buffer size
    // is only 16. The coefficient set is split into 4 groups (mentally...)
    // and the filter computation is cycling through these groups, one
    // group per 44k1 sample:
    //
    // 44k1 sample instant 0 (ui44kSampleCounter == 0):
    //   - Put new 11k sample into the interpolator queue
    //   - Compute the filter using coefficients 0, 4, 8 ... 56, 60
    //
    // 44k1 sample instant 1 (ui44kSampleCounter == 1):
    //   - Compute the filter using coefficients 1, 5, 9 ... 57, 61
    //
    // 44k1 sample instant 2 (ui44kSampleCounter == 2):
    //   - Compute the filter using coefficients 2, 6, 10 ... 58, 62
    //
    // 44k1 sample instant 3 (ui44kSampleCounter == 3):
    //   - Compute the filter using coefficients 3, 7, 11 ... 59, 63
    //
    // 44k1 sample instant 4: (now it starts over again!)
    //   - Put new 11k sample into the interpolator queue
    //   - Compute the filter using coefficients 0, 4, 8 ... 56, 60
    //
    //  etc. etc.

    rI44k = rQ44k = 0.0;           // clear MAC accumulators
    i = ui44kSampleCounter; // 0..3, 0 just after a new value has been inserted
    do  // loop from newest to oldest sample (they are stored "backwards")
    {
      rTmp = rDecIntCoeffs[i];
      rI44k += rInterpolatorQueueI[iInterpolatorIdxIn] * rTmp;
      rQ44k += rInterpolatorQueueQ[iInterpolatorIdxIn] * rTmp;
      if (++iInterpolatorIdxIn >= 16)
        iInterpolatorIdxIn = 0;
      i += 4;
    } while (i < 64); // last i == 60, 61, 62 or 63 depending on coeff' group used

    rI44k *= 4.0e-8;      // normalize (FIR gain ~10^8 !!!)
    rQ44k *= 4.0e-8;

    // Write interleaved samples to output buffer

    *out++ = rI44k * g_rOutputGainFactor;  // left
    *out++ = rQ44k * g_rOutputGainFactor;  // right

    // Do FFT of original input signal for the full spectrum view

    iFftBufIdxIn &= (FFT_SIZE_IN-1);
    g_fltFftBufRe[iFftBufIdxIn++] = leftInput; //rI44k leftInput

    if (iFftBufIdxIn == FFT_SIZE_IN)
    { // time to do the next FFT :

      iFftBufIdxIn = 0;   // don't do it again until buffer is filled

      dspmath_MultiplyHanningWindow (g_fltFftBufRe, FFT_SIZE_IN);
      //dspmath_MultiplyHammingWindow (g_fltFftBufRe, FFT_SIZE_IN);
      //dspmath_MultiplyBlackmanWindow (g_fltFftBufRe, FFT_SIZE_IN);

      dspmath_CalcRealFft (FFT_SIZE_IN,  g_fltFftBufRe, g_fltFftBufIm);

      if (g_qGUIFinishedPlotting)
      {
        g_qGUIFinishedPlotting = FALSE;

        // Note: Less than half of all computed FFTs will be
        // displayed because of the 50ms timer tick but who cares...

        for (i = 0; i < NFFTPLOTPOINTS; ++i)
        {
          re =  g_fltFftBufRe[i];
          im =  g_fltFftBufIm[i];
          // A pure input sin wave ... Asin(wt)... will produce an fft output
          //   peak of (N*A/4)^2  where N is FFT_SIZE .
          // Notes:
          //  - Leave the time-consuming logarithm for the display thread !
          //  - This 'power'-proportional thingy depends on the FFT-size !
          //  - The above formula is only valid for HANN window, not here..

#ifdef SMOOTH_BINS
//          Run a simple first order "RC" filter on each
//          frequency bin for smoothing:
//            out(N) = out(N-1) + K*( in(N) - out(N-1) )
//          where the R*C time constant is proportional to K/FS.
//          A sliding average would have been better (probably) but this
//          is so simple to implement:

          rTmp = SMOOTH_K * (sqrt (re*re + im*im) - g_fltPlotFftV[i]);
          g_fltPlotFftV[i] += rTmp;
#else
          g_fltPlotFftV[i] =  sqrt (re*re + im*im);
#endif
        }
      } // end if( g_qGUIFinishedPlotting )
    } // end if < time to calculate another FFT >
  } // end sample picking for loop

  if (g_qShowCpuLoad)
    g_rCpuLoad = Pa_GetCPULoad (g_PAStream);


  return 0; // a non-zero return will stop the stream
}


/*------------------------------------------------------------------------------
 *
 *      Reaction to WM_PAINT message
 */

#define SPECFRAMEWIDTH  600
#define SPECFRAMEHEIGHT 400
#define CURVEHEIGHT     221
#define CURVESTARTX     60
#define CURVESTARTY     40+CURVEHEIGHT

#define SPECTRUM_MIN_DB -110.0
#define SPECTRUM_MAX_DB 0.0

LONG OnWmPaint (HWND hWnd)
{
  HDC hdcMem;
  HBITMAP hbmMem, hbmOld;
  HPEN hPen, hOldPen;
  PAINTSTRUCT ps;
  RECT rctClientArea;
  int y, iXLoop;
  float rdB;
  float rPixelsPerdB;
  POINT plotPoints[NFFTPLOTPOINTS];
  int i;
  char sz80[84];
  int iNumVisibleChars;
  static int kHzXOffs[23] = { // somewhat faster than a runtime calculation...
    0, 23, 46, 70, 93, 116, 139, 163, 186, 209, 232, 255,
    279, 302, 325, 348, 372, 395, 418, 441, 464, 488, 511
  };

  BeginPaint (hWnd, &ps);

  // Get the size of the program window's client area:
  //
  // NOTE! The coordinates returned by GetClientRect() are:
  //
  //   rctClientArea.left == rctClientArea.top == 0 (always)
  //
  //   rctClientArea.right = WIDTH of client area
  //   (rightmost visible X == rctClientArea.right-1
  //
  //   rctClientArea.bottom = HEIGHT of client area
  //   (bottommost visible Y == rctClientArea.bottom-1

  GetClientRect (hWnd, &rctClientArea);

  // Create a compatible device context (DC) for drawing off-screen:

  hdcMem = CreateCompatibleDC (((LPPAINTSTRUCT)(&ps))->hdc);

  // Create an off-screen bitmap to do the off-screen drawing on:

  hbmMem = CreateCompatibleBitmap (((LPPAINTSTRUCT)(&ps))->hdc,
                                   rctClientArea.right-rctClientArea.left,
                                   rctClientArea.bottom-rctClientArea.top);

  // Direct all drawing operations onto the off-screen bitmap:

  hbmOld = SelectObject (hdcMem, hbmMem);

  // We can now begin the drawing operations, first erase the background:

//  hbrBkGnd = CreateSolidBrush (GetSysColor(COLOR_WINDOW));
//  FillRect(hdcMem, &rctClientArea, hbrBkGnd);
//  DeleteObject(hbrBkGnd);
  FillRect (hdcMem, &rctClientArea, (HBRUSH)GetStockObject(BLACK_BRUSH));

//  SetBkMode(hdcMem, TRANSPARENT);       // alternatives: TRANSPARENT/OPAQUE
  SetBkColor (hdcMem, 0x00000000);        // background for TextOut()

  // Draw status information above the spectrum area:

  SetTextAlign (hdcMem, TA_LEFT);
  SetTextColor(hdcMem, 0x000000FF);
  iNumVisibleChars = sprintf (sz80, "Local Oscillator: %.0f Hz", g_rLoFrequency);
  TextOut (hdcMem, CURVESTARTX, CURVESTARTY-CURVEHEIGHT+1-8-23, sz80, iNumVisibleChars);
  SetTextColor(hdcMem, 0x00FFFF00);
  if (g_iFilterBW == 0)
    i = 300;
  else
  if (g_iFilterBW == 1)
    i = 1000;
  else
    i = 2400;
  iNumVisibleChars = sprintf (sz80, "Filter BW: %d Hz", i);
  TextOut (hdcMem, CURVESTARTX+192, CURVESTARTY-CURVEHEIGHT+1-8-23, sz80, iNumVisibleChars);
  SetTextColor(hdcMem, 0x0000FFFF);
  iNumVisibleChars = sprintf (sz80, "Output Gain: %c%d dB",
                              (g_iOutputGainStep != 0) ? '+' : 32,
                              g_iOutputGainStep*10);
  TextOut (hdcMem, CURVESTARTX+334, CURVESTARTY-CURVEHEIGHT+1-8-23, sz80, iNumVisibleChars);

  // Draw [SAQ], [HELP] and [ABOUT] buttons

  // yes, this is messy, and there are hardcoded coordinates in the
  // WM_LBUTTONDOWN message case too... will do it in a better organized
  // way later...

  SetTextAlign (hdcMem, TA_CENTER);
  SetTextColor(hdcMem, 0x00F0C080);
  hPen = CreatePen (PS_SOLID, 1, 0x00A07000);
  hOldPen = (HPEN)SelectObject (hdcMem, hPen);
    MoveToEx (hdcMem, CURVESTARTX+525, CURVESTARTY-40-160, NULL);
    LineTo (hdcMem, CURVESTARTX+525+42, CURVESTARTY-40-160);
    LineTo (hdcMem, CURVESTARTX+525+42, CURVESTARTY-160);
    LineTo (hdcMem, CURVESTARTX+525, CURVESTARTY-160);
    LineTo (hdcMem, CURVESTARTX+525, CURVESTARTY-40-160);
    TextOut (hdcMem, CURVESTARTX+525+21, CURVESTARTY-38-160, "Tune", 4);
    TextOut (hdcMem, CURVESTARTX+525+21, CURVESTARTY-18-160, "S A Q", 5);

    MoveToEx (hdcMem, CURVESTARTX+525, CURVESTARTY-20-40, NULL);
    LineTo (hdcMem, CURVESTARTX+525+42, CURVESTARTY-20-40);
    LineTo (hdcMem, CURVESTARTX+525+42, CURVESTARTY-40);
    LineTo (hdcMem, CURVESTARTX+525, CURVESTARTY-40);
    LineTo (hdcMem, CURVESTARTX+525, CURVESTARTY-20-40);
    TextOut (hdcMem, CURVESTARTX+525+21, CURVESTARTY-18-40, "Help", 4);

    MoveToEx (hdcMem, CURVESTARTX+525, CURVESTARTY-20, NULL);
    LineTo (hdcMem, CURVESTARTX+525+42, CURVESTARTY-20);
    LineTo (hdcMem, CURVESTARTX+525+42, CURVESTARTY);
    LineTo (hdcMem, CURVESTARTX+525, CURVESTARTY);
    LineTo (hdcMem, CURVESTARTX+525, CURVESTARTY-20);
    TextOut (hdcMem, CURVESTARTX+525+21, CURVESTARTY-18, "About", 5);
  SelectObject (hdcMem, hOldPen);
  DeleteObject (hPen);

  // Draw [-] and [+] buttons for output gain control

  if (g_iOutputGainStep == 0)
    hPen = CreatePen (PS_SOLID, 1, 0x00808080);
  else
    hPen = CreatePen (PS_SOLID, 1, 0x0000FFFF);
  hOldPen = (HPEN)SelectObject (hdcMem, hPen);
    MoveToEx (hdcMem, CURVESTARTX+472, CURVESTARTY-CURVEHEIGHT+1-8-23, NULL);
    LineTo (hdcMem, CURVESTARTX+472+16,  CURVESTARTY-CURVEHEIGHT+1-8-23);
    LineTo (hdcMem, CURVESTARTX+472+16,  CURVESTARTY-CURVEHEIGHT+1-8-23+16);
    LineTo (hdcMem, CURVESTARTX+472,     CURVESTARTY-CURVEHEIGHT+1-8-23+16);
    LineTo (hdcMem, CURVESTARTX+472,     CURVESTARTY-CURVEHEIGHT+1-8-23);
    MoveToEx (hdcMem, CURVESTARTX+472+4, CURVESTARTY-CURVEHEIGHT+1-8-23+8, NULL);
    LineTo (hdcMem, CURVESTARTX+472+13,  CURVESTARTY-CURVEHEIGHT+1-8-23+8);
  SelectObject (hdcMem, hOldPen);
  DeleteObject (hPen);

  if (g_iOutputGainStep == MAXOUTPUTGAINSTEP)
    hPen = CreatePen (PS_SOLID, 1, 0x00808080);
  else
    hPen = CreatePen (PS_SOLID, 1, 0x0000FFFF);
  hOldPen = (HPEN)SelectObject (hdcMem, hPen);
    MoveToEx (hdcMem, CURVESTARTX+472+24, CURVESTARTY-CURVEHEIGHT+1-8-23, NULL);
    LineTo (hdcMem, CURVESTARTX+472+16+24, CURVESTARTY-CURVEHEIGHT+1-8-23);
    LineTo (hdcMem, CURVESTARTX+472+16+24, CURVESTARTY-CURVEHEIGHT+1-8-23+16);
    LineTo (hdcMem, CURVESTARTX+472+24,    CURVESTARTY-CURVEHEIGHT+1-8-23+16);
    LineTo (hdcMem, CURVESTARTX+472+24,    CURVESTARTY-CURVEHEIGHT+1-8-23);
    MoveToEx (hdcMem, CURVESTARTX+472+4+24, CURVESTARTY-CURVEHEIGHT+1-8-23+8, NULL);
    LineTo (hdcMem, CURVESTARTX+472+13+24,  CURVESTARTY-CURVEHEIGHT+1-8-23+8);
    MoveToEx (hdcMem, CURVESTARTX+472+8+24, CURVESTARTY-CURVEHEIGHT+1-8-23+4, NULL);
    LineTo (hdcMem, CURVESTARTX+472+8+24,  CURVESTARTY-CURVEHEIGHT+1-8-23+13);
  SelectObject (hdcMem, hOldPen);
  DeleteObject (hPen);

  // Draw vertical dB labels and ticks:

  SetTextAlign (hdcMem, TA_RIGHT);
  SetTextColor(hdcMem, 0x00008080);
  hPen = CreatePen (PS_SOLID, 1, 0x00008080);
  hOldPen = (HPEN)SelectObject (hdcMem, hPen);
    for (i = 0; i <= 11; i++)
    {
      iNumVisibleChars = sprintf (sz80, "%d", -10*i);
      TextOut (hdcMem, CURVESTARTX-11-5, CURVESTARTY-CURVEHEIGHT+1-8+20*i, sz80, iNumVisibleChars);
      MoveToEx (hdcMem, CURVESTARTX-10-5, CURVESTARTY-CURVEHEIGHT+1+20*i, NULL);
      LineTo (hdcMem, CURVESTARTX-1-5, CURVESTARTY-CURVEHEIGHT+1+20*i);
    }
    SetTextColor(hdcMem, 0x0080FFFF);
    TextOut (hdcMem, CURVESTARTX-11-5, CURVESTARTY-CURVEHEIGHT+1-8-23, "[dB]", 4);

    // Draw horizontal kHz labels and ticks:

    SetTextColor(hdcMem, 0x00008080);
    SetTextAlign (hdcMem, TA_CENTER);
    for (i = 0; i <= 22; i++)
    {
      iNumVisibleChars = sprintf (sz80, "%d", i);
      TextOut (hdcMem, CURVESTARTX+kHzXOffs[i], CURVESTARTY+11+5, sz80, iNumVisibleChars);
      MoveToEx (hdcMem, CURVESTARTX+kHzXOffs[i], CURVESTARTY+2+5, NULL);
      LineTo (hdcMem, CURVESTARTX+kHzXOffs[i], CURVESTARTY+11+5);
    }
    SetTextColor(hdcMem, 0x0080FFFF);
    SetTextAlign (hdcMem, TA_LEFT);
    TextOut (hdcMem, CURVESTARTX+528, CURVESTARTY+11+5, "[kHz]", 5);

    // Draw the dark yellow box around the spectrum plot area:

    MoveToEx (hdcMem, CURVESTARTX-1-5,   CURVESTARTY+1+5, NULL);
    LineTo (hdcMem, CURVESTARTX+1+512+5, CURVESTARTY+1+5);
    LineTo (hdcMem, CURVESTARTX+1+512+5, CURVESTARTY-CURVEHEIGHT+1-1-5);
    LineTo (hdcMem, CURVESTARTX-1-5,     CURVESTARTY-CURVEHEIGHT+1-1-5);
    LineTo (hdcMem, CURVESTARTX-1-5,     CURVESTARTY+1+5);
  SelectObject (hdcMem, hOldPen);
  DeleteObject (hPen);

  // Draw dark gray dotted raster lines:

  hPen = CreatePen (PS_DOT, 1, 0x00404040L);
  hOldPen = (HPEN)SelectObject (hdcMem, hPen);
    for (i = 1; i <= 10; i++)
    {
      MoveToEx (hdcMem, CURVESTARTX, CURVESTARTY-CURVEHEIGHT+1+20*i, NULL);
      LineTo (hdcMem, CURVESTARTX+512, CURVESTARTY-CURVEHEIGHT+1+20*i);
    }

    for (i = 1; i <= 21; i++)
    {
      MoveToEx (hdcMem, CURVESTARTX+kHzXOffs[i], CURVESTARTY, NULL);
      LineTo (hdcMem, CURVESTARTX+kHzXOffs[i], CURVESTARTY-CURVEHEIGHT+1);
    }
  SelectObject (hdcMem, hOldPen);
  DeleteObject (hPen);

  // Draw outer dark gray frame:

  hPen = CreatePen (PS_SOLID, 1, 0x00404040L);
  hOldPen = (HPEN)SelectObject (hdcMem, hPen);
    MoveToEx (hdcMem, CURVESTARTX,   CURVESTARTY, NULL);
    LineTo (hdcMem, CURVESTARTX+512, CURVESTARTY);
    LineTo (hdcMem, CURVESTARTX+512, CURVESTARTY-CURVEHEIGHT+1);
    LineTo (hdcMem, CURVESTARTX,     CURVESTARTY-CURVEHEIGHT+1);
    LineTo (hdcMem, CURVESTARTX,     CURVESTARTY);
  SelectObject (hdcMem, hOldPen);
  DeleteObject (hPen);

  // Draw Filter passband

  i = (int)(g_rLoFrequency*512.0/22050.0 + 0.5);  // VFO offset in pixels

  hPen = CreatePen (PS_SOLID, 1, 0x00808000L);
  hOldPen = (HPEN)SelectObject (hdcMem, hPen);
    if (g_iFilterBW == 0)
    {
      // Approximated passband shape:
      //   Passband width = 300 Hz
      //   Passband center = VFO+700 Hz
      //   Stopband width = 500 Hz
      //   X scale = 22050/512 = ~43.0664 Hz/pixel
      MoveToEx (hdcMem, CURVESTARTX+i+11, CURVESTARTY, NULL);
      LineTo (hdcMem, CURVESTARTX+i+13, CURVESTARTY-CURVEHEIGHT+1);
      LineTo (hdcMem, CURVESTARTX+i+20, CURVESTARTY-CURVEHEIGHT+1);
      LineTo (hdcMem, CURVESTARTX+i+22, CURVESTARTY);
    }
    else
    if (g_iFilterBW == 1)
    {
      // Approximated passband shape:
      //   Passband width = 1000 Hz
      //   Passband center = VFO+700 Hz
      //   Stopband width = 1400 Hz
      //   X scale = 22050/512 = ~43.0664 Hz/pixel
      MoveToEx (hdcMem, CURVESTARTX+i, CURVESTARTY, NULL);
      LineTo (hdcMem, CURVESTARTX+i+5, CURVESTARTY-CURVEHEIGHT+1);
      LineTo (hdcMem, CURVESTARTX+i+28, CURVESTARTY-CURVEHEIGHT+1);
      LineTo (hdcMem, CURVESTARTX+i+33, CURVESTARTY);
    }
    else  // g_iFilterBW == 2
    {
      // Approximated passband shape:
      //   Passband width = 2400 Hz
      //   Passband center = VFO+1500 Hz
      //   Stopband width = 3000 Hz
      //   X scale = 22050/512 = ~43.0664 Hz/pixel
      MoveToEx (hdcMem, CURVESTARTX+i, CURVESTARTY, NULL);
      LineTo (hdcMem, CURVESTARTX+i+7, CURVESTARTY-CURVEHEIGHT+1);
      LineTo (hdcMem, CURVESTARTX+i+63, CURVESTARTY-CURVEHEIGHT+1);
      LineTo (hdcMem, CURVESTARTX+i+70, CURVESTARTY);
    }
  SelectObject (hdcMem, hOldPen);
  DeleteObject (hPen);

  // Draw VFO line

  hPen = CreatePen (PS_SOLID, 1, 0x000000FFL);
  hOldPen = (HPEN)SelectObject (hdcMem, hPen);
    MoveToEx (hdcMem, CURVESTARTX+i, CURVESTARTY, NULL);
    LineTo (hdcMem, CURVESTARTX+i, CURVESTARTY-CURVEHEIGHT+1);
  SelectObject (hdcMem, hOldPen);
  DeleteObject (hPen);

  // Draw spectrum plot:

  hPen = CreatePen (PS_SOLID, 1, 0x0000FF10L);
  hOldPen = (HPEN)SelectObject (hdcMem, hPen);

  rPixelsPerdB = (float)(CURVEHEIGHT) / (SPECTRUM_MAX_DB-SPECTRUM_MIN_DB);

  for (iXLoop = 0; iXLoop < NFFTPLOTPOINTS; iXLoop++)
  {
    plotPoints[iXLoop].x = CURVESTARTX + iXLoop;

    rdB = g_fltPlotFftV[iXLoop];
    if (rdB <= 0)   // array init'ed to zero and an FFT bin *may* be zero too...
      rdB = 1.0e-9; // ...so don't shock that log10 function :~)
    rdB = 20.0 * log10 (rdB * (4.0/FFT_SIZE_IN)); // scaled for Hanning

    y = (int)(rPixelsPerdB * (rdB - SPECTRUM_MIN_DB));
    y = CURVESTARTY - y;

    // Clip Y
    if (y < (CURVESTARTY-CURVEHEIGHT+1))  // shouldn't ever happen if scaled OK...
      y = CURVESTARTY-CURVEHEIGHT+1;
    if (y > CURVESTARTY)  // ...but this *will* happen with a quiet soundcard
      y = CURVESTARTY;

    plotPoints[iXLoop].y = y;
  } // end for X...

  Polyline (hdcMem, plotPoints, NFFTPLOTPOINTS);

  SelectObject (hdcMem, hOldPen);
  DeleteObject (hPen);

  // Draw overload indicator on top of the other graphics

  if (q_OverLoad)
  {
    q_OverLoad = 0; // will stay for at least 50ms (except when dragging VFO)
    SetTextColor(hdcMem, 0x000000FF);
    SetTextAlign (hdcMem, TA_CENTER);
    TextOut (hdcMem, CURVESTARTX+256, CURVESTARTY-CURVEHEIGHT/2,
             "*** O V E R L O A D ***", 23);
  }

  // Show CPU load

  if (g_qShowCpuLoad)
  {
    SetTextColor (hdcMem, 0x00808080);
    SetTextAlign (hdcMem, TA_LEFT);
    iNumVisibleChars = sprintf (sz80, "CPU load = %3.1f %%", 100.0*g_rCpuLoad);
    TextOut (hdcMem, 10, 291, sz80, iNumVisibleChars);
  }

  // "Blit" the finished drawing to the physical screen:

  BitBlt(((LPPAINTSTRUCT)(&ps))->hdc,
         rctClientArea.left, rctClientArea.top,
         rctClientArea.right-rctClientArea.left,
         rctClientArea.bottom-rctClientArea.top,
         hdcMem,
         0, 0,
         SRCCOPY);

  // Done with off-screen bitmap and DC, get rid of them

  SelectObject (hdcMem, hbmOld);
  DeleteObject (hbmMem);
  DeleteDC (hdcMem);

  EndPaint(hWnd, &ps);

  g_qGUIFinishedPlotting = TRUE; // allow audio-thread to fill another buffer
    // ( very crude to do that in a WM_PAINT handler, but keeps it simple )

  return 0L;          // WM_PAINT message sucessfully handled
}


/*------------------------------------------------------------------------------
 *
 *      Increase/decrease output gain
 */

void vChangeOutputGain (int iDirection)
{
  if (iDirection)
  {
    g_iOutputGainStep++;
    if (g_iOutputGainStep > MAXOUTPUTGAINSTEP)
      g_iOutputGainStep = MAXOUTPUTGAINSTEP;
  }
  else
  {
    g_iOutputGainStep--;
    if (g_iOutputGainStep < 0)
      g_iOutputGainStep = 0;
  }

  switch (g_iOutputGainStep)
  { // 0..50dB in 10 db steps
    case 0: g_rOutputGainFactor = 1.0;        break;
    case 1: g_rOutputGainFactor = 3.16227766; break;
    case 2: g_rOutputGainFactor = 10.0;       break;
    case 3: g_rOutputGainFactor = 31.6227766; break;
    case 4: g_rOutputGainFactor = 100.0;      break;
    case 5: g_rOutputGainFactor = 316.227766; break;
  }
}


/*------------------------------------------------------------------------------
 *
 *      Set LO frequency and phase increment
 */

void vSetLoFreq (float rFreq)
{
  g_rLoFrequency = rFreq;

  if (g_iFilterBW == 0)
  {
    if (g_rLoFrequency > MAX_LO_FREQ_NARROW)
      g_rLoFrequency = MAX_LO_FREQ_NARROW;
  }
  else
  if (g_iFilterBW == 1)
  {
    if (g_rLoFrequency > MAX_LO_FREQ_MEDIUM)
      g_rLoFrequency = MAX_LO_FREQ_MEDIUM;
  }
  else
  {
    if (g_rLoFrequency > MAX_LO_FREQ_WIDE)
      g_rLoFrequency = MAX_LO_FREQ_WIDE;
  }

  if (g_rLoFrequency < MIN_LO_FREQ)
    g_rLoFrequency = MIN_LO_FREQ;

  g_rLoPhInc = (g_rLoFrequency * 2.0 * C_PI / FS);
}


/*------------------------------------------------------------------------------
 *
 *      Change LO frequency
 */

void vQSY (float rOffset)
{
  vSetLoFreq (g_rLoFrequency + rOffset);
}


/*------------------------------------------------------------------------------
 *
 *      Set default SAQ settings
 */

void vSetSaqDefults (void)
{
  g_iFilterBW = 1;
  vSetLoFreq (16500.0);
}


/*------------------------------------------------------------------------------
 *
 *      Show About box
 */

void vShowAboutBox (HWND hwnd)
{
  MessageBox (hwnd,
              "SAQ Panoramic VLF Receiver\n\n"
              "Version 0.6\n"
              "Compiled 2007-02-18\n\n"
              "Written by Johan Bodin SM6LKM\n\n"
              "Special thanks to:\n"
              "  Wolfgang B�scher DL4YHF\n"
              "  Alberto di Bene I2PHD",
              "About", 0);
}


/*------------------------------------------------------------------------------
 *
 *      Show Help box
 */

void vShowHelpBox (HWND hwnd)
{
  MessageBox (hwnd,
              "TUNING:\n"
              "   1. Drag the passband area with the left mouse button or\n"
              "   2. Rotate the mouse wheel (*) or\n"
              "   3. Use the arrow keys (*)\n"
              "   4. Press U/D for 1 Hz steps up/down\n\n"

              "   (*) Normal step size is 100 Hz, press Shift for 1kHz steps or "
                "press Ctrl for 10 Hz steps.\n\n"

              "FILTER SELECTION:\n"
              "   1. Right-click anywhere in the window to cycle through the three filter bandwidths or\n"
              "   2. Press N/M/W to select NARROW (300 Hz) / MEDIUM (1000 Hz) / WIDE (2400 Hz).\n\n"

              "   The filters MEDIUM and NARROW are centered around an audio frequency of 700 Hz (CW pitch).\n\n"

              "TUNE IN SAQ ON 17.2 kHz:\n"
              "   1. Left-click the [Tune SAQ] button or\n"
              "   2. Press the Home key.\n\n"

              "   This will set the local oscillator to 16500 Hz and select the MEDIUM filter. "
                "This receiver uses Upper Sideband mode only.\n\n"

              "INPUT OVERLOAD INDICATOR:\n"
              "   *** O V E R L O A D *** will flash in the middle of the screen "
                "if the input signal reaches 1dB below clipping level.\n\n"

              "OUTPUT GAIN SELECTION:\n"
              "   1. Left-click the [-] or [+] button or\n"
              "   2. Press -/+ to decrease/increase the output gain in 10dB steps.\n\n"

              "CPU LOAD INDICATOR:\n"
              "  Press C to toggle CPU load indicator on/off.\n\n"

              "NOTES:\n"
              "   This program uses Windows DEFAULT soundcard for both input and output. "
                " If you have more than one soundcard\n"
              "   installed, please check in the Windows Control Panel which card is selected "
                "as default for sound I/O.\n\n"

              "   Use your Windows RECORDING mixer to select the appropriate input"
                " (LINE, MIC etc.) and for adjusting the input level.\n"
              "   If your RECORDING mixer has a slider called \"Output Mix\", or similar, make sure "
                "it is fully off to prevent echo/oscillation.\n"
              "   Select WAVE output and listening volume in your PLAYBACK mixer. "
                "This program uses the LEFT input only.\n\n"

              "WARNING:\n"
              "   Never connect a large antenna, such as an outdoor longwire, directly to the "
                "soundcard input without adequate protection!\n"
              "   Use at least a series resistor followed by a pair of parallel diodes, connected "
                "back-to-back, across the input.\n"
              "   Make sure that you know what you are doing. You are using this program at your own risk. "
                "You have been warned.",

              "How to use the SAQ Panoramic VLF Receiver:", 0);
}


/*------------------------------------------------------------------------------
 *
 *      Windows message callback handler
 *
 *      (This function is called by the Windows function's DispatchMessage() )
 */


LRESULT CALLBACK WindowProcedure (HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
  int i;
  PaError PaInitresultCode;
  float rDelta;
  int iKeyRepeatCount;
  int iFilterMinX;
  int iFilterMaxX;
  int iMouseY;
  int iMouseX;
  int iVfoX;
  char szErr80[84];
  static int qShiftIsDown = 0;
  static int qCtrlIsDown = 0;
  static int qMouseQSY = 0;
  static int iMouseClickX;
  static float rMouseClickVfoFreq;

  switch (message)
  {
    case WM_CREATE:

      // Init' some variables

      for (i = 0; i < (sizeof(g_fltFftBufRe)/sizeof(float)); ++i)
        g_fltFftBufRe[i] = g_fltFftBufIm[i] = 0.0;

      for (i = 0; i < NFFTPLOTPOINTS; ++i)
        g_fltPlotFftV[i] = 0.0;

      g_qGUIFinishedPlotting = TRUE; // allow audio thread to fill another buffer

      // Initialize PortAudio subsystem

      PaInitresultCode = Pa_Initialize ();
      if (PaInitresultCode == paNoError)
      {
        PaInitresultCode = Pa_OpenDefaultStream (
            &g_PAStream,
            2, 2,       // stereo input and output
            paFloat32,  // most convenient format on a fast machine...
            FS,         // sampling frequency
            FPB, 0,     // FPB frames per buffer, let PA determine numBuffers
            AudioCallback, // callback function
            NULL);      // we have no user data pointer to pass to the callback for now
        if (PaInitresultCode == paNoError)
          PaInitresultCode = Pa_StartStream (g_PAStream);
      }

      if (PaInitresultCode != paNoError)
      {
        Pa_Terminate();
        if (PaInitresultCode == paHostError)
        {
          sprintf (szErr80, "Host error: 0x%08X", Pa_GetHostError ());
          MessageBox (hwnd, szErr80, "SAQrx", MB_APPLMODAL | MB_OK | MB_ICONEXCLAMATION);
          PostQuitMessage (1);
        }
        else
        {
          MessageBox (hwnd, Pa_GetErrorText(PaInitresultCode), "SAQrx",
                      MB_APPLMODAL | MB_OK | MB_ICONEXCLAMATION);
          PostQuitMessage (1);
        }
      }

      // Start a periodic timer that posts a WM_TIMER message each 50ms

      SetTimer (hwnd, 0, 50, NULL);
      break;

    case WM_DESTROY:
      KillTimer (hwnd, 0);          // what if SetTimer() was never called? *#*
      Pa_StopStream (g_PAStream);   // result ignored...
      Pa_CloseStream (g_PAStream);
      Pa_Terminate ();
      PostQuitMessage(0);
      break;

    case WM_TIMER:
      InvalidateRect (hwnd,   // window handle
                      NULL,   // CONST RECT*, NULL => force redraw of entire client area
                      FALSE); // FALSE => do not erase when BeginPaint() is called
      return 0L;

    case WM_ERASEBKGND:       // *#* is this case really needed?
        return (LRESULT)1;    // fake "message handled"

    case WM_PAINT:
      return OnWmPaint (hwnd);

    case WM_RBUTTONDOWN:
      if (g_iFilterBW <= 0)
        g_iFilterBW = 2;
      else
        g_iFilterBW--;
      vQSY (0.0);   // don't QSY unless LO is too high for this filter
      return 0L;

    case WM_MOUSEWHEEL:
      rDelta = 100.0;
      if (wParam & MK_CONTROL)
        rDelta = 10.0;
      if (wParam & MK_SHIFT)
        rDelta = 1000.0;
      if (GET_WHEEL_DELTA_WPARAM (wParam) < 0)
        rDelta = -rDelta;
      vQSY (rDelta);
      return 0L;

    case WM_CHAR:
      switch (wParam)
      {
        case '-':
          vChangeOutputGain (0);
          break;

        case '+':
          vChangeOutputGain (1);
          break;

        case '?':
          vShowAboutBox (hwnd);
          break;

        case 'C':
        case 'c':
          if (g_qShowCpuLoad)
            g_qShowCpuLoad = 0;
          else
            g_qShowCpuLoad = 1;
          break;

        case 'N':
        case 'n':
          g_iFilterBW = 0;
          break;

        case 'M':
        case 'm':
          g_iFilterBW = 1;
          vQSY (0.0);   // don't QSY unless LO is too high for this filter
          break;

        case 'W':
        case 'w':
          g_iFilterBW = 2;
          vQSY (0.0);   // don't QSY unless LO is too high for this filter
          break;

        case 'U':
        case 'u':
          vQSY (1.0);
          break;

        case 'D':
        case 'd':
          vQSY (-1.0);
          break;
      }
      return 0L;

    case WM_KEYUP:
      switch (wParam)
      {
        case VK_SHIFT:
          qShiftIsDown = 0;
          break;

        case VK_CONTROL:
          qCtrlIsDown = 0;
          break;
      }
      return 0L;

    case WM_KEYDOWN:
      iKeyRepeatCount = (int)(lParam & 0xFFFF);
      switch (wParam)
      {
        case VK_SHIFT:
          qShiftIsDown = 1;
          break;

        case VK_CONTROL:
          qCtrlIsDown = 1;
          break;

        case VK_LEFT:
        case VK_DOWN:
          if (qShiftIsDown)
            vQSY (-1000.0*iKeyRepeatCount);
          else
          if (qCtrlIsDown)
            vQSY (-10.0*iKeyRepeatCount);
          else
            vQSY (-100.0*iKeyRepeatCount);
          break;

        case VK_RIGHT:
        case VK_UP:
          if (qShiftIsDown)
            vQSY (1000.0*iKeyRepeatCount);
          else
          if (qCtrlIsDown)
            vQSY (10.0*iKeyRepeatCount);
          else
            vQSY (100.0*iKeyRepeatCount);
          break;

        case VK_HOME: // set default VFO & filter for SAQ reception
          vSetSaqDefults ();
          break;

        case VK_F1:
          vShowHelpBox (hwnd);
      }
      return 0L;

    case WM_LBUTTONDOWN:
      // Warning! Hardcoded coordinates here that must match
      // the corresponding hardcoded stuff in OnWmPaint()!
      iMouseX = LOWORD(lParam);
      iMouseY = HIWORD(lParam);
      // Mouse X in [SAQ], [Help] and [About] button X range?
      if ((iMouseX >= CURVESTARTX+525) &&
          (iMouseX <= (CURVESTARTX+525+42)))
      {
        if ((iMouseY >= CURVESTARTY-40-160) && (iMouseY <= CURVESTARTY-160))
          vSetSaqDefults ();
        else
        if ((iMouseY >= CURVESTARTY-20-40) && (iMouseY <= CURVESTARTY-40))
          vShowHelpBox (hwnd);
        else
        if ((iMouseY >= CURVESTARTY-20) && (iMouseY <= CURVESTARTY))
          vShowAboutBox (hwnd);
      }
      else
      // Mouse Y in spectrum plot Y range?
      if ((iMouseY <= CURVESTARTY) &&
          (iMouseY >= (CURVESTARTY-CURVEHEIGHT+1)))
      {
        iFilterMinX = CURVESTARTX + (int)(g_rLoFrequency*512.0/22050.0 + 0.5);
        if (g_iFilterBW == 0)
        {
          iFilterMinX += 11;
          iFilterMaxX = iFilterMinX + 11;
        }
        else
        if (g_iFilterBW == 1)
        {
          iFilterMaxX = iFilterMinX + 33;
        }
        else
        {
          iFilterMaxX = iFilterMinX + 70;
        }
        // Mouse clicked in the current filter passband?
        if ((iMouseX >= iFilterMinX) && (iMouseX <= iFilterMaxX))
        {
          qMouseQSY = 1;
          iMouseClickX = iMouseX;
          rMouseClickVfoFreq = g_rLoFrequency;
        }
      }
      else
      // Mouse Y in output gain button Y range?
      if ((iMouseY >= (CURVESTARTY-CURVEHEIGHT+1-8-23)) &&
          (iMouseY <= (CURVESTARTY-CURVEHEIGHT+1-8-23+16)))
      {
        // Output gain [-] pressed?
        if ((iMouseX >= (CURVESTARTX+472)) &&
            (iMouseX <= (CURVESTARTX+472+16)))
          vChangeOutputGain (0);
        else
        // Output gain [+] pressed?
        if ((iMouseX >= (CURVESTARTX+472+24)) &&
            (iMouseX <= (CURVESTARTX+472+16+24)))
          vChangeOutputGain (1);
      }
      return 0L;

    case WM_LBUTTONUP:
      qMouseQSY = 0;
      return 0L;

    case WM_MOUSEMOVE:
      if (qMouseQSY)
      {
        iMouseX = LOWORD(lParam);
        vSetLoFreq (rMouseClickVfoFreq +
          (float)(iMouseX - iMouseClickX) * 22050.0 / 512.0);
        InvalidateRect (hwnd, NULL, FALSE); // update screen fast when dragging
      }
      return 0L;

    default:          /* for messages that we don't deal with */
      return DefWindowProc (hwnd, message, wParam, lParam);
  }

  return DefWindowProc (hwnd, message, wParam, lParam);
}


/*------------------------------------------------------------------------------
 *
 *      Window$ main()
 */

const char g_szClassName[] = "SaqPanRx";

int WINAPI WinMain (HINSTANCE hThisInstance,
                    HINSTANCE hPrevInstance,
                    LPSTR lpszArgument,
                    int nShowHow )
{
  HWND hwnd;                // this is the handle for our window
  MSG messages;             // here messages to the application are saved
  WNDCLASSEX wc;            // data structure for the windowclass

  if (FindWindow (g_szClassName, NULL) != NULL)
  {
    if (MessageBox (NULL, "An instance of SAQrx is already running!\n"
                          "Start another SAQrx anyway?", "Oops!",
        MB_ICONEXCLAMATION | MB_YESNO | MB_DEFBUTTON2) == IDNO)
    return 0;
  }



  // Set up the main window

  wc.cbSize        = sizeof (WNDCLASSEX);
  wc.style         = CS_HREDRAW | CS_VREDRAW; // or 0?
  wc.lpfnWndProc   = WindowProcedure;
  wc.cbClsExtra    = 0 ;
  wc.cbWndExtra    = 0 ;
  wc.hInstance     = hThisInstance;
  wc.hIcon         = LoadIcon(hThisInstance,TEXT("PROGRAM_ICON"));
  wc.hCursor       = LoadCursor (NULL, IDC_HAND);
  wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1); // without "+1"?
  wc.lpszMenuName  = NULL;          // no menu from a RESOURCE !
  wc.lpszClassName = g_szClassName;
  wc.hIconSm       = LoadIcon(hThisInstance,TEXT("PROGRAM_ICON"));

  if (!RegisterClassEx (&wc))
  {
    MessageBox (NULL, "Window Registration Failed!", "Error!",
                MB_ICONEXCLAMATION | MB_OK);
    return 0;
  }

  // The class is registered, let's create the program

  hwnd = CreateWindowEx (
           0,                             // use WS_EX_CLIENTEDGE for "sunken edge" (else 0)
           g_szClassName,                 // window class name
           "SAQ Panoramic VLF Receiver v0.6",  // title Text
           WS_CAPTION | WS_SYSMENU,       // fixed size window with close button [X] only
           CW_USEDEFAULT,                 // Windows decides the initial X position
           CW_USEDEFAULT,                 // Windows decides the initial Y position
           640,                           // The window's width INCLUDING BORDER FRAME!
           340,                           // The window's heigth INCLUDING BORDER FRAME!
           HWND_DESKTOP,                  // The window is a child-window to desktop
           NULL,                          // No menu
           hThisInstance,                 // Program instance handle
           NULL                           // No Window creation data
           );

  if (hwnd == NULL)
  {
    MessageBox (NULL, "Window Creation Failed!", "Error!",
                MB_ICONEXCLAMATION | MB_OK);
    return 0;
  }

  // Make the window visible on the screen

  ShowWindow (hwnd, nShowHow );
//  UpdateWindow (hwnd); not needed since we use a timer tick for redraw

  // Run the message loop. It will run until GetMessage() returns 0

  while (GetMessage (&messages, NULL, 0, 0) > 0)
  {
    // Translate virtual-key messages into character messages
    TranslateMessage (&messages);
    // Send message to WindowProcedure
    DispatchMessage (&messages);
  }

  // The program return-value is 0 - The value that PostQuitMessage() gave
  return messages.wParam;
}
