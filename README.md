# SAQrx v0.6 genuine

**SAQrx** is a Software Defined [VLF](https://en.wikipedia.org/wiki/Very_low_frequency) receiver developped in 2006/2007 by [Johan H. Bodin SM6LKM](https://github.com/JohBod) using:
 - [DEV-C++](http://dev-cpp.com/) v4.9.9.2 (aka 5.0): and IDE for C/C++ dev.
 - [PortAudio](http://www.portaudio.com/) v18: an audio I/O library.

The code for v0.6 is only available [here](https://sites.google.com/site/sm6lkm/saqrx-vlf-receiver).
To preserve this work in a more sustainable place, I'm also publishing it here.

This is NOT my work.

For more information see this [presentation](https://gitlab.com/jeayne/saqrx/-/blob/main/README.md)
